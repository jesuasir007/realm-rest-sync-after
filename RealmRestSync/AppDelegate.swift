//
//  AppDelegate.swift
//  RealmRestSync
//
//  Created by Stefan Kofler on 26.05.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import UIKit
import RealmSwift

let API = "https://sampleservice.com/api"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var syncService: SyncService!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        syncService = SyncService(modelTypes: [User.self])
        return true
    }

}

